#!/bin/bash

path=""
url=""

while IFS= read -r line
do
    if [[ $line == *"path = "* ]]
    then
        path=$(echo $line | cut -d '=' -f 2 | xargs)
    fi

    if [[ $line == *"url = "* ]]
    then
        url=$(echo $line | cut -d '=' -f 2 | xargs)
    fi

    if [[ -n $path && -n $url ]]
    then
        git clone $url $path

        path=""
        url=""
    fi
done < .gitmodules

